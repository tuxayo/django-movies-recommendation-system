import json
import urllib.request
import movies_app.recommendations as recommendations

from bs4 import BeautifulSoup
from django.shortcuts import render
from django.conf import settings

from .models import Movie, Category, Rating, User, Occupation


def home(request):
    return render(request, 'movies_app/home.html')


def movies(request):
    categories = Category.objects.all()
    category_to_filter = request.GET.get('category')
    movies_filtered = _filter_movies(category_to_filter)
    context = {'movies': movies_filtered,
               'categories': categories,
               'current_category': category_to_filter}
    return render(request, 'movies_app/movies.html', context)


def search_similar_movies(request):
    return render(request, 'movies_app/search_similar_movies.html')


def similar_movies(request, movie_id):
    movie = Movie.objects.get(id=movie_id)
    user_ratings_dict = _get_all_users_ratings_dict()
    movies = recommendations.topMatches(recommendations.transformPrefs(user_ratings_dict),
                                        movie.title,
                                        n=3)
    return render(request, 'movies_app/similar_movies.html',
                  {'movies': movies})


def users(request):
    occupations = Occupation.objects.all()
    occupation_to_filter = request.GET.get('occupation')
    users_filtered = _filter_users(occupation_to_filter)
    context = {'users': users_filtered,
               'occupations': occupations,
               'current_occupation': occupation_to_filter}
    return render(request, 'movies_app/users.html', context)


def search_ratings_by_user(request):
    return render(request, 'movies_app/search_ratings_by_user.html')


def user_ratings(request, user_id):
    user = User.objects.get(id=user_id)
    ratings = user.rating_set.all()

    return render(request, 'movies_app/user_ratings.html',
                  {'ratings': ratings})


def recommend_movies_to_user(request):
    return render(request, 'movies_app/recommend_movies_to_user.html')


def user_recommendations(request, user_id):
    user_ratings_dict = _get_all_users_ratings_dict()
    itemsim = _get_item_similarities()
    recommended_movies = recommendations.getRecommendedItems(user_ratings_dict,
                                                             itemsim,
                                                             user_id)[0:2]
    recommended_movies = _add_categories_to_recommended_movies(recommended_movies)
    return render(request, 'movies_app/user_recommendations.html',
                  {'recommended_movies': recommended_movies})


def search_similar_users(request):
    return render(request, 'movies_app/search_similar_users.html')


def similar_users(request, user_id):
    user_ratings_dict = _get_all_users_ratings_dict()
    users = recommendations.topMatches(user_ratings_dict,
                                       user_id,
                                       n=4)
    users = _add_occupation_to_similar_users(users)
    return render(request, 'movies_app/similar_users.html',
                  {'users': users})


def top_rated_movies(request):
    # top movies with average rating
    # returns Rating objects
    top_rated_movies = Rating.objects.raw(
        'SELECT id, movie_id, COUNT(*), AVG(rating) ' +
        'FROM movies_app_rating ' +
        'GROUP BY movie_id ' +
        'ORDER BY AVG(rating) ' +
        'DESC LIMIT 5')
    titles = _extract_titles(top_rated_movies)
    descriptions = _fetch_movies_descriptions([rating.movie for rating in top_rated_movies])
    ratings = _extract_average_ratings(top_rated_movies)
    top_movies_titles_descriptions_ratings = zip(titles, descriptions, ratings)

    return render(request, 'movies_app/top_rated_movies.html',
                  {'top_movies_titles_descriptions_ratings':
                   top_movies_titles_descriptions_ratings})


def _add_categories_to_recommended_movies(movies):
    return map(_add_categories_to_recommended_movie, movies)


def _add_categories_to_recommended_movie(recommended_movie):
    rating, title = recommended_movie
    movie = Movie.objects.get(title=title)
    categories = str(movie.categories.all())
    return (title, rating, categories)


def _add_occupation_to_similar_users(users):
    # return [_add_occupation_to_similar_user(user) for user in users]
    return map(_add_occupation_to_similar_user, users)


def _add_occupation_to_similar_user(similar_user):
    similarity, user_id = similar_user
    user = User.objects.get(id=user_id)
    occupation = user.occupation
    return (user_id, similarity, occupation)


def _filter_movies(category_to_filter):
    if category_to_filter is None or category_to_filter == '':
        return Movie.objects.all()

    # example of a filter in a many to many relationship
    category_to_filter_id = Category.objects.get(name=category_to_filter).id
    # it seems like 'categories=' mean 'categories contains'
    return Movie.objects.filter(categories=category_to_filter_id)


def _filter_users(occupation_to_filter):
    if occupation_to_filter is None or occupation_to_filter == '':
        return User.objects.all()
    occupation_to_filter_id = Occupation.objects.get(name=occupation_to_filter).id
    return User.objects.filter(occupation=occupation_to_filter_id)


def _fetch_movies_descriptions(movies):
    return [_fetch_movie_description(movie.imdb_url) for movie in movies]


def _fetch_movie_description(imdb_url):
    print('fetching description:', imdb_url)
    page_HTML = _download_page(imdb_url)
    soup = BeautifulSoup(page_HTML, 'html.parser')
    selector_matches = soup.select('#titleStoryLine [itemprop=description] p')
    if selector_matches == []:
        return "imdb_url doesn't point to a movie page"
    [description_node] = selector_matches
    description_text = description_node.next
    return description_text


def _download_page(url):
    response = urllib.request.urlopen(url)
    data = response.read()
    return data.decode('utf-8')


def _extract_average_ratings(results):
    return [getattr(result, 'AVG(rating)') for result in results]


def _extract_titles(results):
    return [result.movie.title for result in results]


def _get_all_users_ratings_dict():
    with open(settings.USER_RATINGS_DICT_PATH, 'r') as file:
        user_ratings_dict_json = file.readline()
    return json.loads(user_ratings_dict_json)


def _get_item_similarities():
    with open(settings.ITEM_SIMILARITIES_PATH, 'r') as file:
        item_similarities_json = file.readline()
    return json.loads(item_similarities_json)
