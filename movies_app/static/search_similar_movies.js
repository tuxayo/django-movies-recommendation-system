function redirectToSimilarMovies() {
    var movie_id = document.getElementById("movie_id").value;
    window.location = "/movies/" + movie_id + "/similar";
}
