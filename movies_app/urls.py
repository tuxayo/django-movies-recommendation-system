from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),

    url(r'^movies/$', views.movies, name='movies'),
    url(r'^top-rated-movies/$',
        views.top_rated_movies, name='top_rated_movies'),
    url(r'^search-similar-movies/$',
        views.search_similar_movies, name='search_similar_movies'),
    url(r'^movies/(?P<movie_id>[0-9]+)/similar/$',
        views.similar_movies, name='similar_movies'),

    url(r'^users/$', views.users, name='users'),
    url(r'^search-ratings-by-user/$',
        views.search_ratings_by_user, name='search_ratings_by_user'),
    url(r'^users/(?P<user_id>[0-9]+)/ratings/$',
        views.user_ratings, name='user_ratings'),
    url(r'^recommend-movies-to-user/$',
        views.recommend_movies_to_user, name='recommend_movies_to_user'),
    url(r'^users/(?P<user_id>[0-9]+)/recommendations/$',
        views.user_recommendations, name='user_recommendations'),
    url(r'^search-similar-users/$',
        views.search_similar_users, name='search_similar_users'),
    url(r'^users/(?P<user_id>[0-9]+)/similar/$',
        views.similar_users, name='similar_users'),
]
