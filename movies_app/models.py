from django.db import models


class Occupation(models.Model):
    name = models.CharField(max_length=70)

    def __str__(self):
        return ' '.join([
            self.name
        ])


class User(models.Model):
    occupation = models.ForeignKey(Occupation, on_delete=models.PROTECT)
    age = models.IntegerField()
    gender = models.CharField(max_length=40)
    postcode = models.CharField(max_length=30)

    def __str__(self):
        return ' '.join([
            str(self.id),
            str(self.occupation),
            str(self.age),
            self.gender,
            self.postcode,
        ])


class Category(models.Model):
    name = models.CharField(max_length=70)

    # def movies(self):  # doesn't work, returns None :-(
    #     self.movie_set.all()

    def __str__(self):
        return ' '.join([
            self.name
        ])


class Movie(models.Model):
    title = models.CharField(max_length=70)
    release_date = models.DateField(null=True)
    video_release_date = models.DateField(null=True)
    imdb_url = models.URLField(max_length=300)
    categories = models.ManyToManyField(Category)

    def __str__(self):
        return self.title


class Rating(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    rating = models.IntegerField()
