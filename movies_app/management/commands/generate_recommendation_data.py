import json
import sys
from django.conf import settings
from django.core.management.base import BaseCommand

from movies_app.models import Movie, User

import movies_app.recommendations as recommendations


class Command(BaseCommand):
    help = 'Generate and save user ratings dictionnary and item similarities'

    def handle(self, *args, **options):
        user_ratings_dict = _user_ratings_dict()
        _item_similarities(user_ratings_dict)


def _item_similarities(user_ratings_dict):
    print('generating item similarities')
    item_similarities = recommendations.calculateSimilarItems(
        user_ratings_dict, n=50)

    item_similarities_json = json.dumps(item_similarities)
    with open(settings.ITEM_SIMILARITIES_PATH, 'w+') as file:
        file.write(item_similarities_json)


def _user_ratings_dict():
    user_ratings_dict = _get_all_users_ratings_dict()
    user_ratings_dict_json = json.dumps(user_ratings_dict)
    with open(settings.USER_RATINGS_DICT_PATH, 'w+') as file:
        file.write(user_ratings_dict_json)
    return user_ratings_dict


def _get_all_users_ratings_dict():
    user_ratings_dict = {}
    print('generating users_ratings_dict')
    users = User.objects.all()
    for user in users:
        ratings = user.rating_set.all()
        user_ratings_dict.setdefault(user.id, {})
        for rating in ratings:
            title = Movie.objects.get(id=rating.movie_id).title
            user_ratings_dict[user.id][title] = rating.rating
        print('.', end='')
        sys.stdout.flush()
    print('\n', end='')
    return user_ratings_dict
