import os
import io
import datetime
from dateutil.parser import parse
from django.conf import settings
from django.core.management.base import BaseCommand
from movies_app.models import User, Movie, Occupation, Category, Rating
from django.db import transaction


class Command(BaseCommand):
    help = 'Imports MovieLens data from TODO'

    def handle(self, *args, **options):
        self._clean_data()
        self._import_data()

    def _clean_data(self):
        self.stdout.write('Cleaning data')
        Movie.objects.all().delete()
        User.objects.all().delete()
        Occupation.objects.all().delete()
        Category.objects.all().delete()
        Rating.objects.all().delete()

    def _import_data(self):
        self.stdout.write('Importing data')
        _import_file('u.genre', _import_category_line)
        _import_file('u.occupation', _import_occupation_line)
        _import_file('u.user', _import_user_line)
        _import_file('u.item', _import_movie_line)
        _import_file('u.data', _import_rating_line)


def _import_file(file_name, line_importer_func):
    data_folder = os.path.join(settings.BASE_DIR,
                               'movies_app',
                               'movie_lens_data')
    file_path = os.path.join(data_folder, file_name)
    with io.open(file_path, encoding='cp1252') as f:
        lines = f.read().splitlines()
        with transaction.atomic():
            for line in lines:
                line_importer_func(line)


def _import_category_line(line):
    if line == '':
        return
    category_name = line.split('|')[0]
    category_id = line.split('|')[1]
    Category.objects.create(id=category_id, name=category_name)


def _import_occupation_line(line):
    Occupation.objects.create(name=line)


def _import_user_line(line):
    user_as_list = line.split('|')
    id = user_as_list[0]
    age = user_as_list[1]
    gender = user_as_list[2]
    occupation = user_as_list[3]
    postcode = user_as_list[4]
    # makes a lot of queries but doesn't seen to be really slow for now
    occupation_id = Occupation.objects.filter(name=occupation)[0].id

    User.objects.create(id=id,
                        age=age,
                        gender=gender,
                        occupation_id=occupation_id,
                        postcode=postcode)


def _import_rating_line(line):
    rating_as_list = line.split('\t')
    user_id = rating_as_list[0]
    movie_id = rating_as_list[1]
    rating_score = rating_as_list[2]
    timestamps_str = rating_as_list[3]
    date = datetime.date.fromtimestamp(int(timestamps_str))
    Rating.objects.create(user_id=user_id,
                          movie_id=movie_id,
                          date=date,
                          rating=rating_score)


def _import_movie_line(line):
    movie = line.split('|')
    id = movie[0]
    title = movie[1]
    release_date = movie[2]
    video_release_date = movie[3]
    imdb_url = movie[4]
    release_date = _import_date(release_date)
    video_release_date = _import_date(video_release_date)
    movie_object = Movie.objects.create(id=id,
                                        title=title,
                                        release_date=release_date,
                                        video_release_date=video_release_date,
                                        imdb_url=imdb_url)

    categories = movie[5:]
    _add_categories_to_movie(categories, movie_object)


def _add_categories_to_movie(categories, movie):
    # example of categories param value
    # ['0','0','1','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0',0']
    categories_indices = _find_indices(categories, lambda elem: elem == '1')
    for cat_index in categories_indices:
        movie.categories.add(cat_index)


def _import_date(date_str):
    if date_str == '':
        return None
    return parse(date_str)


def _find_indices(lst, condition):
    return [i for i, elem in enumerate(lst) if condition(elem)]
