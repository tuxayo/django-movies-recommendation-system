from django.contrib import admin
from .models import User, Movie, Occupation, Category, Rating

# Register your models here.
admin.site.register(User)
admin.site.register(Movie)
admin.site.register(Occupation)
admin.site.register(Category)
admin.site.register(Rating)
